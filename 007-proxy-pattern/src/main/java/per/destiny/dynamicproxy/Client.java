package per.destiny.dynamicproxy;

import java.lang.reflect.Proxy;

public class Client {

    public static void main(String[] args) {
        Subject realSubject = new RealSubject();
        DynamicProxy proxy = new DynamicProxy(realSubject);
        Subject subject = (Subject) Proxy.newProxyInstance(realSubject.getClass().getClassLoader(),new Class[]{Subject.class},proxy);
        subject.visit();
    }

}
