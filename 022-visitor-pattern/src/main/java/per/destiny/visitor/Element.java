package per.destiny.visitor;

/**
 * 抽象元素类
 */
public abstract class Element {

    public abstract void accept(IVisitor visitor);

    public abstract void doSomething();
}
