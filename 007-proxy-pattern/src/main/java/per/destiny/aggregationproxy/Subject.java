package per.destiny.aggregationproxy;

public interface Subject {

    void visit();
}
