package per.destiny.cglibproxy;

import net.sf.cglib.core.DebuggingClassWriter;

public class Client {

	public static void main(String[] args) {
		//CGlib动态代理类保存至本地
		System.setProperty(DebuggingClassWriter.DEBUG_LOCATION_PROPERTY, "D:\\class");
		CGLibProxy proxy = new CGLibProxy();
		Train train = (Train) proxy.getProxy(Train.class);
		train.move();
	}
}
