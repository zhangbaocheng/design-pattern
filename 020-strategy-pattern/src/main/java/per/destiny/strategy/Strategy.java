package per.destiny.strategy;

/**
 * 抽象策略角色
 */
public interface Strategy {

    /**
     * 策略方法
     */
    void strategyInterface();
}
