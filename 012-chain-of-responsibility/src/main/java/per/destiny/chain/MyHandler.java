package per.destiny.chain;

public class MyHandler extends AbstractHandler implements Handler {

    private String name;

    public MyHandler(String name){
        this.name = name;
    }

    @Override
    public void operator() {
        System.err.println(name + "deal!");
        if(getHandler() != null ){
            getHandler().operator();
        }
    }
}
