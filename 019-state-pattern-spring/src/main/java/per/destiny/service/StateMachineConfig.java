package per.destiny.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import per.destiny.constants.LeaveStateEnum;
import per.destiny.constants.LeaveStateEvent;

import java.util.EnumSet;

/**
 * 状态机配置类
 */
@Configuration
@EnableStateMachine
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<LeaveStateEnum, LeaveStateEvent> {

    @Override
    public void configure(StateMachineStateConfigurer<LeaveStateEnum, LeaveStateEvent> states) throws Exception {
        // 初始化状态
        states.withStates().initial(LeaveStateEnum.WAIT_SUBMIT).states(EnumSet.allOf(LeaveStateEnum.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<LeaveStateEnum, LeaveStateEvent> transitions) throws Exception {
        // 定义各个状态之间转换时对应的动作（事件）
        transitions.withExternal().source(LeaveStateEnum.WAIT_SUBMIT).target(LeaveStateEnum.WAIT_LEADER_APPROVAL).event(LeaveStateEvent.SUBMIT)
                .and().withExternal().source(LeaveStateEnum.WAIT_LEADER_APPROVAL).target(LeaveStateEnum.APPROVAL_SUCCEEDED).event(LeaveStateEvent.PASS)
                .and().withExternal().source(LeaveStateEnum.WAIT_LEADER_APPROVAL).target(LeaveStateEnum.APPROVAL_FAILED).event(LeaveStateEvent.FAILED);
    }

    @Override
    public void configure(StateMachineConfigurationConfigurer<LeaveStateEnum, LeaveStateEvent> config) throws Exception {
        // 设置状态机id
        config.withConfiguration().machineId("leaveStateMachine");
    }
}
