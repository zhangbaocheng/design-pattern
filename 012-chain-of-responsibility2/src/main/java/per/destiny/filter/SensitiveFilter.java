package per.destiny.filter;

import per.destiny.chain.FilterChain;
import per.destiny.dto.Request;
import per.destiny.dto.Response;

public class SensitiveFilter implements Filter {

    @Override
    public void doFilter(Request req, Response rep, FilterChain chain) {
        System.out.println("处理了敏感字符替换" + req.getMsg());
        chain.doFilter(req, rep);
    }
}
