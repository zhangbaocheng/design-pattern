package per.destiny.component;

/**
 * 表示叶节点对象。叶子节点没有子节点。
 */
public class Leaf extends Component {

    public Leaf(String name) {
        super(name);
    }

    @Override
    public void add(Component component) {
        System.err.println("can not add to a leaf");
    }

    @Override
    public void remove(Component component) {
        System.out.println("Can not remove from a leaf");
    }

    @Override
    public void display(int depth) {
        String temp = "";
        for (int i = 0;i<depth;i++){
            temp += '-';
        }
        System.err.println(temp + name);
    }
}
