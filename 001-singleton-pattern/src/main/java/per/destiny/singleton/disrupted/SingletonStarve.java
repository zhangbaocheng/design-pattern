package per.destiny.singleton.disrupted;

/**
 * 饿汉模式
 */
public class SingletonStarve {

    private volatile static SingletonStarve singleton = new SingletonStarve();

    private SingletonStarve(){
    }


    public static SingletonStarve newInstance(){
        return singleton;
    }
}
