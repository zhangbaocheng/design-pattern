package per.destiny.flyweight;

import java.util.HashMap;
import java.util.Map;

/**
 * 亨元工厂类
 */
public class FlyweightFactory {

    private static Map<String,Shape> shapes = new HashMap<>();

    public static Shape getShape(String color){
        Shape shape = shapes.get(color);
        if(null == shape){
            shape = new Circle(color);
            shapes.put(color,shape);
        }
        return shape;
    }

    public static int getSum(){
        return shapes.size();
    }
}
