public class Student {

    private String name;
    private int age;
    private String school;

    private Student(StudentBuilder studentBuilder){
        this.name = studentBuilder.name;
        this.age = studentBuilder.age;
        this.school = studentBuilder.school;
    }

    public static StudentBuilder createBuilder(){
        return new StudentBuilder();
    }

    static class StudentBuilder{
        private String name;
        private int age;
        private String school;

        public StudentBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public StudentBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public StudentBuilder setSchool(String school) {
            this.school = school;
            return this;
        }

        Student builder(){
            return new Student(this);
        }
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getSchool() {
        return school;
    }
}
