package per.destiny.facade;

/**
 * Memory子系统类
 */
public class Memory {

    public void start(){
        System.err.println("memory starts...");
    }

    public void shutDown(){
        System.err.println("memory shutDowns...");
    }
}
