package per.destiny.visitor;

/**
 * 元素类
 */
public class ConcreteElementB extends Element {
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public void doSomething() {
        System.err.println("这是元素B");
    }
}
