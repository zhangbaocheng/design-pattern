package per.destiny.extendsproxy;

public class ProxySubject extends RealSubject {
    @Override
    public void visit() {
        System.err.println("代理之前");
        super.visit();
        System.err.println("代理之后");
    }
}
