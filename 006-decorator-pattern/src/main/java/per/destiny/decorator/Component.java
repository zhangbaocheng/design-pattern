package per.destiny.decorator;

public interface Component {
    void operate();
}
