package per.destiny.aggregationproxy;

/**
 * 聚合式静态代理
 */
public class Client {

    public static void main(String[] args) {
        ProxySubject subject = new ProxySubject(new RealSubject());
        subject.visit();
    }
}
