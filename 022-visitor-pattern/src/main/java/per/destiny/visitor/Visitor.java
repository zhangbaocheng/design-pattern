package per.destiny.visitor;

/**
 * 访问者
 */
public class Visitor implements IVisitor {
    @Override
    public void visit(ConcreteElementA concreteElementA) {
        concreteElementA.doSomething();
    }

    @Override
    public void visit(ConcreteElementB concreteElementB) {
        concreteElementB.doSomething();
    }
}
