package per.destiny.singleton.disrupted;

import java.io.Serializable;

/**
 * 饿汉模式
 */
public class SingletonStarveSer implements Serializable{

    private static final long serialVersionUID = 1L;

    private volatile static SingletonStarveSer singleton = new SingletonStarveSer();

    private SingletonStarveSer(){
    }


    public static SingletonStarveSer newInstance(){
        return singleton;
    }
}
