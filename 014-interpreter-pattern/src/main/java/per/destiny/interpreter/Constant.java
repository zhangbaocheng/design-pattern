package per.destiny.interpreter;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 一个Constant对象代表一个布尔常量
 */
@EqualsAndHashCode
@ToString
public class Constant implements Expression {

    private boolean value;

    public Constant(boolean value){
        this.value = value;
    }

    @Override
    public boolean interpret(Context ctx) {
        return value;
    }


}
