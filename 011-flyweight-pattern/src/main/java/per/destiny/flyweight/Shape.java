package per.destiny.flyweight;

/**
 * 形状类
 */
public abstract class Shape {
    public abstract void draw();
}
