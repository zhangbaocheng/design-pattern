package per.destiny.decorator;

public class DecoratorB extends Decorator {

    public DecoratorB(Component component) {
        super(component);
    }

    @Override
    protected void before() {
        System.err.println("B before...");
    }

    @Override
    protected void after() {
        System.err.println("B after...");
    }
}
