package per.destiny.mediator;

/**
 * Created by destinys on 2018/7/1.
 */
public class Manager implements Mediator {

    @Override
    public void change(String message, Employee employee, String name) {
        System.err.println("经理收到" + name + "的需求：" + message);
        System.err.println("经理将" + name + "的需求发送给目标职员");
        employee.called(message,name);
    }
}
