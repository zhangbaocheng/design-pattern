package per.destiny.iterator;

/**
 * 定义迭代器角色
 */
public interface Iterator {

    boolean hasNext();

    Object next();
}
