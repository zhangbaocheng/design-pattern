package per.destiny.facade;

/**
 * 门面类（核心）
 */
public class Computer {
    private CPU cpu;
    private Memory memory;
    private Disk disk;

    public Computer(){
        cpu = new CPU();
        memory = new Memory();
        disk = new Disk();
    }

    public void start(){
        System.err.println("computer start begin");
        cpu.start();
        disk.start();
        memory.start();
        System.err.println("computer start end");
    }

    public void shutDown(){
        System.err.println("computer shutDown start");
        cpu.start();
        disk.shutDown();
        memory.shutDown();
        System.err.println("computer shutDown end");
    }
}
