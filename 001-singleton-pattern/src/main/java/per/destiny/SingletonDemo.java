package per.destiny;

import per.destiny.singleton.enums.SingletonEnum;
import per.destiny.singleton.disrupted.SingletonLazy;
import per.destiny.singleton.disrupted.SingletonStarve;
import per.destiny.singleton.disrupted.SingletonStarveSer;
import per.destiny.singleton.undisrupted.SingletonUnDisrupted;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;

public class SingletonDemo {

    public static void main(String[] args) {
//        lazyPattern();
//        starvePattern();
//        enumPattern();
//        useReflectDestorySington1();
//        useReflectDestorySington2();
//        useReflectDestorySington3();
//        useSerialDestorySingleton();
//        useReflectUnDestorySington1();
        //useReflectUnDestorySington2();
        useSerialUnDestorySington();
    }

    /**
     * 防止序列化破坏单例模式
     */
    private static void useSerialUnDestorySington() {
        SingletonUnDisrupted singletonUnDisrupted1 = SingletonUnDisrupted.newInstance();

        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream("singletonUnDisrupted.obj");
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(singletonUnDisrupted1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try {
                fileOutputStream.close();
                objectOutputStream.close();
            }
            catch (Exception e){

            }
        }


        SingletonUnDisrupted singletonUnDisrupted2 = null;
        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;
        try {
            fileInputStream = new FileInputStream("singletonUnDisrupted.obj");
            objectInputStream = new ObjectInputStream(fileInputStream);
            singletonUnDisrupted2 = (SingletonUnDisrupted) objectInputStream.readObject();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try {
                fileInputStream.close();
                objectInputStream.close();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        System.err.println(singletonUnDisrupted1 == singletonUnDisrupted2);
    }

    /**
     * 防止反射破坏单例模式
     */
    private static void useReflectUnDestorySington1() {
        try {
            Class<?> clazz = SingletonUnDisrupted.class;
            Constructor<?> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            SingletonUnDisrupted singletonUnDisrupted1 =  (SingletonUnDisrupted)constructor.newInstance();
            SingletonUnDisrupted singletonUnDisrupted2 =  (SingletonUnDisrupted)constructor.newInstance();
            System.err.println(singletonUnDisrupted1 == singletonUnDisrupted2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 防止反射破坏单例模式
     */
    private static void useReflectUnDestorySington2() {
        try {
            SingletonUnDisrupted singletonUnDisrupted1 = SingletonUnDisrupted.newInstance();

            Class<?> clazz = SingletonUnDisrupted.class;
            Constructor<?> constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            SingletonUnDisrupted singletonUnDisrupted2 =  (SingletonUnDisrupted)constructor.newInstance();
            System.err.println(singletonUnDisrupted1 == singletonUnDisrupted2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 使用序列化破坏单利
     */
    private static void useSerialDestorySingleton() {
        SingletonStarveSer singletonStarveSer1 = SingletonStarveSer.newInstance();
        writeObj(singletonStarveSer1);
        SingletonStarveSer singletonStarveSer2 = readObj();
        System.err.println("使用序列化破坏单例:" + (singletonStarveSer1 == singletonStarveSer2));

    }

    private static SingletonStarveSer readObj() {
        SingletonStarveSer singletonStarveSer = null;
        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;
        try {
            fileInputStream = new FileInputStream("C:\\Git\\test\\SingletonStarveSer.obj");
            objectInputStream = new ObjectInputStream(fileInputStream);
            singletonStarveSer = (SingletonStarveSer) objectInputStream.readObject();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try {
                fileInputStream.close();
                objectInputStream.close();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        return singletonStarveSer;
    }

    private static void writeObj(SingletonStarveSer singletonStarveSer) {
        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream("C:\\Git\\test\\SingletonStarveSer.obj");
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(singletonStarveSer);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            try {
                fileOutputStream.close();
                objectOutputStream.close();
            }
            catch (Exception e){

            }

        }
    }

    /**
     * 使用枚举方式
     */
    private static void enumPattern() {
        SingletonEnum singletonEnum1 = SingletonEnum.INSTANCE;
        SingletonEnum singletonEnum2 = SingletonEnum.INSTANCE;
        System.err.println("枚举方式创建单例:" + (singletonEnum1 == singletonEnum2));
    }

    /**
     * 饿汉模式
     */
    private static void starvePattern() {
        SingletonStarve singletonStarve1 = SingletonStarve.newInstance();
        SingletonStarve singletonStarve2 = SingletonStarve.newInstance();
        System.err.println("饿汉模式：" + (singletonStarve1 == singletonStarve2));
    }

    /**
     * 懒汉模式
     */
    private static void lazyPattern(){
        SingletonLazy singletonLazy1 = SingletonLazy.newInstance1();
        SingletonLazy singletonLazy2 = SingletonLazy.newInstance1();
        System.err.println("懒汉模式,线程不安全：" + (singletonLazy1 == singletonLazy2));

        SingletonLazy singletonLazy3 = SingletonLazy.newInstance2();
        SingletonLazy singletonLazy4 = SingletonLazy.newInstance2();
        System.err.println("懒汉模式,线程安全：" + (singletonLazy3 == singletonLazy4));
    }

    /**
     * 使用反射破坏单例模式
     */
    private static void useReflectDestorySington1(){
        try {
            Class<?> clazz = SingletonLazy.class;
            Constructor[] constructors = clazz.getDeclaredConstructors();
            for (Constructor constructor: constructors) {
                //无参构造器
                if(0 == constructor.getParameterCount() && clazz.getName().equals(constructor.getName())){
                    constructor.setAccessible(true);
                    SingletonLazy singletonLazy1 = (SingletonLazy)constructor.newInstance();
                    SingletonLazy singletonLazy2 = (SingletonLazy)constructor.newInstance();
                    System.err.println("使用反射破坏单例:" + (singletonLazy1 == singletonLazy2));
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 使用反射破坏单例模式
     */
    private static void useReflectDestorySington2(){
        try {
            Class<?> clazz = SingletonLazy.class;
            //不传参数，获取的是无参构造器
            Constructor constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            SingletonLazy singletonLazy1 = (SingletonLazy)constructor.newInstance();
            SingletonLazy singletonLazy2 = (SingletonLazy)constructor.newInstance();
            System.err.println("使用反射破坏单例:" + (singletonLazy1 == singletonLazy2));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * 使用反射破坏单例模式
     */
    private static void useReflectDestorySington3(){
        try {
            Class<?> clazz = SingletonStarve.class;
            Constructor constructor = clazz.getDeclaredConstructor();
            constructor.setAccessible(true);
            SingletonStarve singletonStarve1 = (SingletonStarve)constructor.newInstance();
            SingletonStarve singletonStarve2 = (SingletonStarve)constructor.newInstance();
            System.err.println("使用反射破坏单例:" + (singletonStarve1 == singletonStarve2));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
