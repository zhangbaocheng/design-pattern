package per.destiny.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;


/**
 * 该类不仅仅可以用于传入目标类，
 * 在一些复杂的应用中（框架等），还可以通过构造器传入其他的内容
 * 在invoke方法中使用
 */
public class DynamicProxy implements InvocationHandler {

    private Object object;

    public DynamicProxy(Object object){
        this.object = object;
    }


    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        Object result = method.invoke(object,args);
        return result;
    }
}
