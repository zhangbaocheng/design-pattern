package per.destiny.simplefactory;

/**
 * 火车
 */
public class Train implements Vehicle{

    @Override
    public void run() {
        System.err.println("train run...");
    }
}
