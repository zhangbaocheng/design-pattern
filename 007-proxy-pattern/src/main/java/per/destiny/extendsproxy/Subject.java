package per.destiny.extendsproxy;

public interface Subject {

    void visit();
}
