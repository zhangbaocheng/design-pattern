package per.destiny.interpreter;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class And implements Expression {

    private Expression left;

    private Expression right;


    public And(Expression left,Expression right){
        this.left = left;
        this.right = right;
    }

    @Override
    public boolean interpret(Context ctx) {
        return left.interpret(ctx) && right.interpret(ctx);
    }



}
