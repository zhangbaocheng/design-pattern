package per.destiny.bridge;

public class Client {

    public static void main(String[] args) {
        Bridge bridge = new MyBridge();
        Driver driver = new MySQLDriver();
        bridge.setDriver(driver);
        bridge.connect();

        driver = new SQLServerDriver();
        bridge.setDriver(driver);
        bridge.connect();
    }
}
