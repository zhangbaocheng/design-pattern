package per.destiny.extendsproxy;

public class RealSubject implements Subject {
    @Override
    public void visit() {
        System.err.println("RealSubject visit...");
    }
}
