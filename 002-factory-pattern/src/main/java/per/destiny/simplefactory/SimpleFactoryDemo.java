package per.destiny.simplefactory;

import java.util.HashMap;
import java.util.Map;

public class SimpleFactoryDemo {

    public static void main(String[] args) {
        useVehicleFactory1();
        useVehicleFactory2();
        useVehicleFactory3();
    }

    /**
     * 使用if，else工厂创建对象
     */
    private static void useVehicleFactory1(){
        Vehicle vehicle = VehicleFactory.createVehicle1("bus");
        vehicle.run();
        vehicle = VehicleFactory.createVehicle1("car");
        vehicle.run();
        vehicle = VehicleFactory.createVehicle1("train");
        vehicle.run();
    }

    /**
     * 根据类的全路径来创建对象
     */
    private static void useVehicleFactory2(){
        Vehicle vehicle = VehicleFactory.createVehicle2("per.destiny.simplefactory.Bus");
        vehicle.run();
        vehicle = VehicleFactory.createVehicle2("per.destiny.simplefactory.Car");
        vehicle.run();
        vehicle = VehicleFactory.createVehicle2("per.destiny.simplefactory.Train");
        vehicle.run();
    }

    /**
     * 根据Map映射来创建对象
     */
    private static void useVehicleFactory3(){
        //可以使用Map，也可以放到配置文件里
        Map<String,String> map = new HashMap<String,String>(){{
            put("bus","per.destiny.simplefactory.Bus");
            put("car","per.destiny.simplefactory.Car");
            put("train","per.destiny.simplefactory.Train");
        }};

        Vehicle vehicle = VehicleFactory.createVehicle2(map.get("bus"));
        vehicle.run();
        vehicle = VehicleFactory.createVehicle2(map.get("car"));
        vehicle.run();
        vehicle = VehicleFactory.createVehicle2(map.get("train"));
        vehicle.run();
    }
}
