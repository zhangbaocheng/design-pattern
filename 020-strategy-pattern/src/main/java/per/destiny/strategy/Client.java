package per.destiny.strategy;

public class Client {
    public static void main(String[] args) {
        Context context1 = new Context(new ConcreteStrategyA());
        context1.contextInterface();

        Context context2 = new Context(new ConcreteStrategyB());
        context2.contextInterface();
    }

}
