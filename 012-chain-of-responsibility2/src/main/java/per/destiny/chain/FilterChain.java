package per.destiny.chain;


import per.destiny.dto.Request;
import per.destiny.dto.Response;
import per.destiny.filter.Filter;

import java.util.ArrayList;
import java.util.List;

public class FilterChain {

    List<Filter> filterlist = new ArrayList<>();
    private int index;

    public FilterChain addFilter(Filter filter) {
        filterlist.add(filter);
        return this;
    }

    public void doFilter(Request request, Response response) {
        if (index == filterlist.size()) {
            return;//这里是逆序处理响应的关键, 当index为容器大小时, 证明对request的处理已经完成, 下面进入对response的处理.
        }
        Filter f = filterlist.get(index);//过滤器链按index的顺序拿到filter
        index++;
        f.doFilter(request, response, this);
    }

}