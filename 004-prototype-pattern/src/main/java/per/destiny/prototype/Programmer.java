package per.destiny.prototype;

import java.io.Serializable;

/**
 * 原型模式，实现对象的clone
 */
public class Programmer implements Serializable,Cloneable{
    private static final long serialVersionUID = -1877875360807481643L;

    private String name;

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Programmer proto = (Programmer) super.clone();
        return proto;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
