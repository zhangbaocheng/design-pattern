package per.destiny.factory;


public interface VehicleFactory {

    Vehicle createVehicle();
}
