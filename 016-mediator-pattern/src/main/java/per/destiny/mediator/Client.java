package per.destiny.mediator;

/**
 * Created by destinys on 2018/7/1.
 */
public class Client {
    public static void main(String[] args) {
        //分配职员与经理
        Mediator manager = new Manager();

        EmpA empA = new EmpA(manager,"职员A");
        EmpB empB = new EmpB(manager,"职员B");
        EmpC empC = new EmpC(manager,"职员C");
        //职员A的需求
        String messageA = "这些资料需要B职员操作";
        empA.call(messageA,empB,empA.getName());

        String messageC = "这些资料需要B职员操作";
        empC.call(messageC,empB,empC.getName());
    }
}
