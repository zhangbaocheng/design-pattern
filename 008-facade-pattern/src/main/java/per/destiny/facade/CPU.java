package per.destiny.facade;

/**
 * CPU子系统类
 */
public class CPU {

    public void start(){
        System.err.println("cpu starts...");
    }

    public void shutDown(){
        System.err.println("cpu shutDowns...");
    }
}
