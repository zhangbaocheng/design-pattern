package per.destiny.singleton.disrupted;

/**
 * 懒汉模式
 */
public class SingletonLazy {

    private volatile static SingletonLazy singleton;

    private SingletonLazy(){
    }


    /**
     * 线程不安全
     * @return
     */
    @Deprecated
    public static SingletonLazy newInstance1(){
        if(null == singleton){
            singleton = new SingletonLazy();
        }
        return singleton;
    }

    /**
     * 线程安全synchronized,但是每次调用该方法都需要加锁，实际情况只需要在没有实例化对象的时候加锁才是有效的。所以这样加锁会影响效率
     * @return
     */
    public static synchronized SingletonLazy newInstance2(){
        if(null == singleton){
            singleton = new SingletonLazy();
        }
        return singleton;
    }

    /**
     * 锁向下移，这样只有在对象为空的时候才会加锁
     * @return
     */
    public static SingletonLazy newInstance3(){
        if(null == singleton){
            synchronized (SingletonLazy.class){
                if(null == singleton){
                    singleton = new SingletonLazy();
                }
            }
        }
        return singleton;
    }

}
