package per.destiny.mediator;

/**
 * 调停者接口
 */
public interface Mediator {

    void change(String message,Employee employee,String name);

}
