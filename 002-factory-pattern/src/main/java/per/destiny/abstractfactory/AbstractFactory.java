package per.destiny.abstractfactory;

/**
 * 其实工厂模式就是一种特殊的抽象工程模式
 *
 * 只不过，工厂模式只能建造一个类别的产品
 *
 * 而抽象工厂模式的工厂可以建造多个类别的产品
 *
 */
public class AbstractFactory {
}
