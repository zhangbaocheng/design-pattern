package per.destiny.template;

/**
 * 汽车模型
 */
public abstract class CarModel {

    /**
     * 汽车启动
     */
    protected abstract void start();

    /**
     * 停车
     */
    protected abstract void stop();

    /**
     * 用户并不需要关注你的车怎么启动或者怎么停下来的，可以开可以听就可以了
     */
    public final void exec(){
        this.start();
        this.stop();
    }
}
