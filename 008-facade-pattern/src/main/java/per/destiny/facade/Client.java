package per.destiny.facade;

public class Client {

    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.start();
        computer.shutDown();
    }
}
