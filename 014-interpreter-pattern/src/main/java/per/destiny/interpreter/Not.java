package per.destiny.interpreter;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Not implements Expression{

    private Expression expression;

    @Override
    public boolean interpret(Context ctx) {
        return !expression.interpret(ctx);
    }
}
