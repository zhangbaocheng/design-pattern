package per.destiny.iterator;

/**
 * 定义容器角色(Aggregate)
 * 定义集合可以的操作
 */
public interface List {

    void add(Object object);

    Object get(int index);

    Iterator iterator();

    int getSize();
}
