package per.destiny.state;

public class Client {

    public static void main(String[] args) {
        Context context = new Context();
        context.setState(new Rain());
        System.err.println(context.stateMessage());
        context.setState(new Sunshine());
        context.stateMessage();
        System.err.println(context.stateMessage());
    }
}
