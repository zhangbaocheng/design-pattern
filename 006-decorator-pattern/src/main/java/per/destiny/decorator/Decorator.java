package per.destiny.decorator;

public abstract class Decorator implements Component {

    private Component component;

    public Decorator(Component component){
        this.component = component;
    }

    protected abstract void before();
    protected abstract void after();

    @Override
    public void operate() {
        before();
        component.operate();
        after();
    }
}
