package per.destiny;

import per.destiny.chain.FilterChain;
import per.destiny.dto.Request;
import per.destiny.dto.Response;
import per.destiny.filter.HttpFilter;
import per.destiny.filter.SensitiveFilter;

public class DemoBox {
    public static void main(String[] args) {
        Request request = getRequest();
        Response response = new Response();//响应

        FilterChain filterChain = new FilterChain();//过滤器链
        filterChain.addFilter(new HttpFilter())
                .addFilter(new SensitiveFilter());

        filterChain.doFilter(request, response);//直接调用过滤器链的doFilter()方法进行处理
    }

    private static Request getRequest() {
        String msg = " 大家好 ";//以下三行模拟一个请求
        Request request = new Request();
        request.setMsg(msg);
        return request;
    }
}