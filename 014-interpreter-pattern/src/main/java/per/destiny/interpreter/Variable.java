package per.destiny.interpreter;

import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 一个Variable对象代表一个有名变量
 */
@EqualsAndHashCode
@ToString
public class Variable implements Expression {

    private String name;

    public Variable(String name){
        this.name = name;
    }

    @Override
    public boolean interpret(Context ctx) {
        return ctx.lookup(this);
    }


}
