package per.destiny.simplefactory;

public class VehicleFactory {

    public static Vehicle createVehicle1(String type){
        Vehicle vehicle = null;
        if("bus".equalsIgnoreCase(type)){
            vehicle = new Bus();
        }
        else if("car".equalsIgnoreCase(type)){
            vehicle = new Car();
        }
        else if("train".equalsIgnoreCase(type)){
            vehicle = new Train();
        }
        return vehicle;
    }

    public static Vehicle createVehicle2(String type){
        Vehicle vehicle = null;
        try {
            Class clazz = Class.forName(type);
            vehicle = (Vehicle) clazz.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return vehicle;
    }
}
