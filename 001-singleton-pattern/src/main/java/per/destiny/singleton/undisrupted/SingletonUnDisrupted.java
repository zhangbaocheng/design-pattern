package per.destiny.singleton.undisrupted;

import java.io.Serializable;

/**
 * 单例模式，不可被破坏
 */
public class SingletonUnDisrupted implements Serializable{

    private static final long serialVersionUID = -266194913434407036L;

    private static boolean flag = false;

    //JVM保证了任何线程在访问静态变量之前一定先创建了此实例
    private static final SingletonUnDisrupted singletonUnDisrupted = new SingletonUnDisrupted();

    private SingletonUnDisrupted(){
        synchronized (SingletonUnDisrupted.class){
            if(!flag){
                flag = !flag;
            }
            else {
                throw new RuntimeException("单例模式被侵犯");
            }
        }

    }

    public static SingletonUnDisrupted newInstance(){
        return singletonUnDisrupted;
    }

    private Object readResolve(){
        return singletonUnDisrupted;
    }

}
