package per.destiny.service;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;
import per.destiny.constants.LeaveStateEnum;
import per.destiny.constants.LeaveStateEvent;
import per.destiny.model.LeaveBill;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 配置状态机对应的动作处理方法
 */
@WithStateMachine(name = "leaveStateMachine")
@Configuration
public class StateMachineEventConfig {

    public Map<Long, LeaveBill> database = new ConcurrentHashMap<Long, LeaveBill>() {{
        put(1L, new LeaveBill(1L, "心情不好，申请请假一天", LeaveStateEnum.WAIT_SUBMIT.ordinal()));
        put(2L, new LeaveBill(2L, "家里有事，申请请假", LeaveStateEnum.WAIT_SUBMIT.ordinal()));
    }};

    @OnTransition(source = "WAIT_SUBMIT", target = "WAIT_LEADER_APPROVAL")
    public void submit(Message<LeaveStateEvent> msg) {
        Long leaveId = (Long) msg.getHeaders().get("leaveId");
        // 从数据库中查询请假单
        LeaveBill leaveBill = database.get(leaveId);
        // 将状态设置为待领导审批
        leaveBill.setState(LeaveStateEnum.WAIT_LEADER_APPROVAL.ordinal());
        // 更新数据库
        database.put(leaveBill.getId(), leaveBill);
        // 通知申请人
        System.out.println("亲爱的xxx,你的请假单已提交审批");
    }

    @OnTransition(source = "WAIT_LEADER_APPROVAL", target = "APPROVAL_SUCCEEDED")
    public void pass(Message<LeaveStateEvent> msg) {
        Long leaveId = (Long) msg.getHeaders().get("leaveId");
        // 从数据库中查询请假单
        LeaveBill leaveBill = database.get(leaveId);
        // 将状态设置为审批通过
        leaveBill.setState(LeaveStateEnum.APPROVAL_SUCCEEDED.ordinal());
        // 更新数据库
        database.put(leaveBill.getId(), leaveBill);
        // 通知申请人
        System.out.println("亲爱的xxx,你的请假单已审批通过");
    }

    @OnTransition(source = "WAIT_LEADER_APPROVAL", target = "APPROVAL_FAILED")
    public void failed(Message<LeaveStateEvent> msg) {
        Long leaveId = (Long) msg.getHeaders().get("leaveId");
        // 从数据库中查询请假单
        LeaveBill leaveBill = database.get(leaveId);
        // 将状态设置为审批未通过
        leaveBill.setState(LeaveStateEnum.APPROVAL_FAILED.ordinal());
        // 更新数据库
        database.put(leaveBill.getId(), leaveBill);
        System.out.println("您好，您的请假单审批未通过");
    }
}
