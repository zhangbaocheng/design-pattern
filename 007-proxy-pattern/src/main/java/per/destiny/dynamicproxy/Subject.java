package per.destiny.dynamicproxy;

public interface Subject {

    void visit();
}
