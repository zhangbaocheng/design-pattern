package per.destiny;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import per.destiny.constants.LeaveStateEnum;
import per.destiny.constants.LeaveStateEvent;

@SpringBootTest
public class ApplicationTests {

    @Autowired
    private StateMachine<LeaveStateEnum, LeaveStateEvent> stateMachine;

    @Test
    public void test() {
        stateMachine.start();
        stateMachine.sendEvent(MessageBuilder.withPayload(LeaveStateEvent.SUBMIT).setHeader("leaveId", 1L).build());
        stateMachine.sendEvent(MessageBuilder.withPayload(LeaveStateEvent.PASS).setHeader("leaveId", 1L).build());
    }
}
