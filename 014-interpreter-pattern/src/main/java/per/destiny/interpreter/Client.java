package per.destiny.interpreter;

public class Client {

    public static void main(String[] args) {
        Context ctx = new Context();
        Variable x = new Variable("x");
        Variable y = new Variable("y");
        Constant c = new Constant(true);
        ctx.assign(x,false);
        ctx.assign(y,true);

        Expression expression = new Or(new And(c,x),new And(y,new Not(x)));
        System.err.println("x = " + x.interpret(ctx));
        System.err.println("y = " + y.interpret(ctx));
        System.err.println(expression.toString() + " = " + expression.interpret(ctx));
    }
}
