package per.destiny.memento;

/**
 * 游戏角色
 */
public class GameRole {
    private int vit;

    private int atk;

    public void init(){
        vit = 100;
        atk = 100;
    }

    public void show(){
        System.err.println("体力：" + vit);
        System.err.println("攻击力：" + atk);
    }

    public void fightBoss(){
        this.vit = 0;
        this.atk = 0;
    }

    public RoleStateMemento saveMemento(){
        return new RoleStateMemento(this.vit,this.atk);
    }

    public void recove(RoleStateMemento roleStateMemento){
        this.vit=roleStateMemento.getVit();
        this.atk=roleStateMemento.getAtk();
    }
}
