package per.destiny.command;

public interface Command {

    void exec();
}
