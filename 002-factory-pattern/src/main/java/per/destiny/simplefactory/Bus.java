package per.destiny.simplefactory;

/**
 * 大巴车
 */
public class Bus implements Vehicle{

    @Override
    public void run() {
        System.err.println("bus run...");
    }
}
