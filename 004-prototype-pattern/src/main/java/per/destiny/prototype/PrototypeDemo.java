package per.destiny.prototype;

public class PrototypeDemo {

    public static void main(String[] args) {
        usePrototype1();

        usePrototype2();
    }

    private static void usePrototype2() {
        try {
            Address address = new Address();
            address.setName("addreddName");
            Student student = new Student();
            student.setAddress(address);

            Student student2 = (Student) student.clone();
            System.err.println(student2);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private static void usePrototype1() {
        try {
            Programmer programmer = new Programmer();
            programmer.setName("name..");
            Programmer programmer2 = (Programmer)programmer.clone();
            System.err.println(programmer2.getName());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}
