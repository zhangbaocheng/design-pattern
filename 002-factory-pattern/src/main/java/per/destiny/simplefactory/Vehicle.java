package per.destiny.simplefactory;

/**
 * 交通工具，接口
 */
public interface Vehicle {

    default void run(){
        System.err.println("run...");
    }
}
