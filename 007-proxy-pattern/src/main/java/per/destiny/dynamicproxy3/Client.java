package per.destiny.dynamicproxy3;

import java.lang.reflect.*;

public class Client {

	public static void main(String[] args)
			throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
		//生成$Proxy0的class文件，保存至/design-pattern/目录下com/sun/proxy下
		System.getProperties().put("sun.misc.ProxyGenerator.saveGeneratedFiles", "true");
		//获取动态代理类
		Class proxyClazz = Proxy.getProxyClass(UserDao.class.getClassLoader(), UserDao.class);

		//获得代理类的构造函数，并传入参数类型InvocationHandler.class
		Constructor constructor = proxyClazz.getConstructor(InvocationHandler.class);


		//通过构造函数来创建动态代理对象，将自定义的InvocationHandler实例传入
		UserDao userDao = (UserDao) constructor.newInstance(new MyInvocationHandler(new UserDaoImpl()));
		//通过代理对象调用目标方法
		userDao.save();
	}


	static class MyInvocationHandler implements InvocationHandler {

		private Object target;

		public MyInvocationHandler(Object target) {
			this.target = target;
		}

		@Override
		public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
			System.out.println("开始.......");
			return method.invoke(target, args);
		}
	}
}
