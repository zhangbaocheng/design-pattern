package per.destiny.factory;

public class ProductDemo {
    public static void main(String[] args) {
        createVehicle(new AirplaneFactory());
        createVehicle(new BikeFactory());
    }

    private static void createVehicle(VehicleFactory vehiclef){
        VehicleFactory vehicleFactory = vehiclef;
        Vehicle vehicle = vehicleFactory.createVehicle();
        vehicle.run();
    }
}
