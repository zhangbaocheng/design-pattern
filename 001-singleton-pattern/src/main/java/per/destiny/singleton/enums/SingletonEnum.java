package per.destiny.singleton.enums;

/**
 * 使用枚举来实现单利，最推荐的做法，其他的做法会因为序列化或者反射而破坏单利
 */
public enum  SingletonEnum {
    INSTANCE;

    private String name;

    public void test(){
        System.err.println("test...");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
