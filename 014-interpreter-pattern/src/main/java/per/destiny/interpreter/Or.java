package per.destiny.interpreter;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Or implements Expression {

    private Expression left;

    private Expression right;


    @Override
    public boolean interpret(Context ctx) {
        return left.interpret(ctx) || right.interpret(ctx);
    }
}
