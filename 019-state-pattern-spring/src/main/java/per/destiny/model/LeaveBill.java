package per.destiny.model;

public class LeaveBill {

    /**
     * 请假单id
     */
    private long id;

    /**
     * 请假原因
     */
    private String reason;

    /**
     * 单据状态
     */
    private int state;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public LeaveBill(long id, String reason, int state) {
        this.id = id;
        this.reason = reason;
        this.state = state;
    }
}
