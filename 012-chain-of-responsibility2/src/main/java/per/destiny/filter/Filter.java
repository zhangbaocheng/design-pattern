package per.destiny.filter;

import per.destiny.chain.FilterChain;
import per.destiny.dto.Request;
import per.destiny.dto.Response;

public interface Filter{
    void doFilter(Request req, Response rep, FilterChain chain);
}