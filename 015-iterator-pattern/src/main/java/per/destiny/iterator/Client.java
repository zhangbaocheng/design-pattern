package per.destiny.iterator;

/**
 * Created by destinys on 2018/7/1.
 */
public class Client {

    public static void main(String[] args) {
        List list = new ConcreteList();
        list.add("one");
        list.add("two");
        list.add("three");

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.err.println("iterator.next() = "+iterator.next());
        }
    }
}
