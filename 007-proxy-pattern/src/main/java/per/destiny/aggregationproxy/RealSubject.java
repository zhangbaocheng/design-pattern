package per.destiny.aggregationproxy;

public class RealSubject implements Subject {

    @Override
    public void visit() {
        System.err.println("RealSubject visit...");
    }
}
