package per.destiny.statedemo;

/**
 * 相当于状态模式中的Context
 */
public class Stage {

    private Actor actor = new HappyActor();

    //改变引用actor的指向的具体类型
    public void change(){
        actor = new SadActor();
    }

    public void performPlay(){
        actor.act();
    }

}
