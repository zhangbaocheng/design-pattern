package per.destiny.simplefactory;

/**
 * 小汽车
 */
public class Car implements Vehicle{

    @Override
    public void run() {
        System.err.println("car run...");
    }
}
