package per.destiny.extendsproxy;

public class Client {
    public static void main(String[] args) {
        ProxySubject proxySubject = new ProxySubject();
        proxySubject.visit();
    }
}
