package per.destiny.component;

/**
 * 声明一个接口用于访问和管理 Component 的子部件
 */
public abstract class Component {
    protected String name;

    public Component(String name){
        this.name = name;
    }

    public abstract void add(Component component);

    public abstract void remove(Component component);

    public abstract void display(int depth);
}
