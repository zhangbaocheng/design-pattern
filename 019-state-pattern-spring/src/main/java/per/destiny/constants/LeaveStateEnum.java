package per.destiny.constants;

/**
 * 请假单状态
 */
public enum LeaveStateEnum {

    /**
     * 待提交
     */
    WAIT_SUBMIT,

    /**
     * 等待领导审批
     */
    WAIT_LEADER_APPROVAL,

    /**
     * 审批成功，请假成功
     */
    APPROVAL_SUCCEEDED,

    /**
     * 万恶的领导不给批假，审批未通过
     */
    APPROVAL_FAILED
}
