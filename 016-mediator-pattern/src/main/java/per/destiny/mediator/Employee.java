package per.destiny.mediator;

/**
 *  职员接口
 */
public abstract class Employee {

    private String name;

    private Mediator mediator;

    public Employee(Mediator mediator,String name){
        this.mediator = mediator;
        this.name = name;
    }

    public void called(String message,String name){
        System.err.println(this.name + "接收到来自" + name + "的需求：" + message);
    }

    public void call(String message,Employee employee,String name){
        System.err.println(name + "发起需求：" + message);
        mediator.change(message,employee,name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
