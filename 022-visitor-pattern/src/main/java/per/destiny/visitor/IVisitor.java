package per.destiny.visitor;

/**
 * 抽象访问者
 */
public interface IVisitor {

    void visit(ConcreteElementA concreteElementA);

    void visit(ConcreteElementB concreteElementB);
}
