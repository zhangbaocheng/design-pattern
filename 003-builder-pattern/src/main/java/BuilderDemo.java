public class BuilderDemo {

    /**
     * 建造者模式（Builder）
     * @param args
     */
    public static void main(String[] args) {
        Student student = Student.createBuilder().setAge(10).setName("张三").setSchool("清华大学").builder();
        System.err.println("student.getName():" + student.getName());
        System.err.println("student.getAge():" + student.getAge());
        System.err.println("student.getSchool():" + student.getSchool());
    }
}
