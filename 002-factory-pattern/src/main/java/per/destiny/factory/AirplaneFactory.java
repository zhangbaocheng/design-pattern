package per.destiny.factory;

public class AirplaneFactory implements VehicleFactory{
    @Override
    public Vehicle createVehicle() {
        return new Airplane();
    }
}
