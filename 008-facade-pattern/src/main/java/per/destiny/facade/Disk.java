package per.destiny.facade;

/**
 * Disk子系统类
 */
public class Disk {

    public void start(){
        System.err.println("disk starts...");
    }

    public void shutDown(){
        System.err.println("disk shutDowns...");
    }
}
