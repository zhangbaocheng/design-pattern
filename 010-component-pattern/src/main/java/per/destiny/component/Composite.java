package per.destiny.component;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {

    private List<Component> children = new ArrayList<>();

    public Composite(String name) {
        super(name);
    }

    @Override
    public void add(Component component) {
        children.add(component);
    }

    @Override
    public void remove(Component component) {
        children.remove(component);
    }

    @Override
    public void display(int depth) {
        String temp = "";
        for(int i = 0;i<depth;i++){
            temp += '-';
        }
        System.err.println(temp + name);

        for (Component component : children){
            component.display(depth + 5);
        }
    }
}
