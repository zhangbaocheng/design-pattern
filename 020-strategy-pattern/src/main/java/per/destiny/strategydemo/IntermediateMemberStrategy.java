package per.destiny.strategydemo;

/**
 * 中级会员折扣实现类
 */
public class IntermediateMemberStrategy implements MemberStrategy {
    @Override
    public double calcPrice(double booksPrice) {
        System.err.println("对于中级会员的折扣为10%");
        return booksPrice * 0.9;
    }
}
