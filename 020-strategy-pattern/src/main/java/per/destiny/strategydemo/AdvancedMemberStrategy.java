package per.destiny.strategydemo;

/**
 * 高级会员折扣实现类
 */
public class AdvancedMemberStrategy implements MemberStrategy {
    @Override
    public double calcPrice(double booksPrice) {
        System.err.println("对于高级会员的折扣为20%");
        return booksPrice * 0.8;
    }
}
