package per.destiny.decorator;

public class Client {

    public static void main(String[] args) {
        Component component = new ConcreteComconent();
        component = new DecoratorA(component);
        component = new DecoratorB(component);
        component.operate();
    }
}
