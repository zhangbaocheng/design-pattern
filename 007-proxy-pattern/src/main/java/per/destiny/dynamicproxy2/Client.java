package per.destiny.dynamicproxy2;

public class Client {

    public static void main(String[] args) {
        UserDao userDao = new UserDaoImpl();
        ProxyFactory proxyFactory = new ProxyFactory(userDao);
        UserDao proxy = (UserDao) proxyFactory.getProxyInstance();
        proxy.save();
    }
}
