package per.destiny.template;

public class Client {
    public static void main(String[] args) {
        //家里的第一辆车，作为用户的我们并不需要关注车怎么启动的，子类变量变为父类，多态
        CarModel wCar = new WCar();
        wCar.exec();

        CarModel oCar = new OCar();
        oCar.exec();
    }
}
