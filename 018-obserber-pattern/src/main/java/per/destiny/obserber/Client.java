package per.destiny.obserber;

public class Client {

    public static void main(String[] args) {
        WechatServer wechatServer = new WechatServer();


        Observer userZhang = new User("zhangsan");
        Observer userLi = new User("LiSi");
        Observer userWang = new User("WangWu");

        wechatServer.registerObserver(userZhang);
        wechatServer.registerObserver(userLi);
        wechatServer.registerObserver(userWang);

        wechatServer.setInfomation("PHP是世界上最好用的语言!");

        System.err.println("-------------------------");

        wechatServer.removeObserver(userZhang);
        wechatServer.setInfomation("JAVA是世界上最好用的语言!");
    }
}
