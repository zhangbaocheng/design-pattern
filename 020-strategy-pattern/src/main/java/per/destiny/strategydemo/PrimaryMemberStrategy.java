package per.destiny.strategydemo;

/**
 * 初级会员折扣实现类
 */
public class PrimaryMemberStrategy implements MemberStrategy{
    @Override
    public double calcPrice(double booksPrice) {
        System.err.println("对于初级会员的没有折扣");
        return booksPrice;
    }
}
