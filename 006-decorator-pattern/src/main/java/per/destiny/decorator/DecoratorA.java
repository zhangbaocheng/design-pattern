package per.destiny.decorator;

public class DecoratorA extends Decorator {

    public DecoratorA(Component component) {
        super(component);
    }

    @Override
    protected void before() {
        System.err.println("A before...");
    }

    @Override
    protected void after() {
        System.err.println("A after...");
    }
}
