package per.destiny.constants;

/**
 * 状态转换的动作
 */
public enum LeaveStateEvent {
    /**
     * 提交审批
     */
    SUBMIT,

    /**
     * 审批通过
     */
    PASS,

    /**
     * 审批不通过
     */
    FAILED
}
