package per.destiny.strategydemo;

public class Client {

    public static void main(String[] args) {
        //选择并创建需要使用的策略对象
        MemberStrategy memberStrategy = new AdvancedMemberStrategy();

        //创建环境
        Price price = new Price(memberStrategy);

        //计算价格
        double quote = price.quote(300);
        System.err.println("图书的最终价格为：" + quote);
    }
}
